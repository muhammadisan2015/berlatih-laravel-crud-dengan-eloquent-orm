@extends('adminlte.master')

@section('content')
      <div class="mt-3 ml-3">
      <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$pertanyaan->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="id">Id</label>
                    <input type="text" class="form-control" id="id" name="id" value="{{ old('id',$pertanyaan->id) }}" placeholder="Enter id">
                    @error('id')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul',$pertanyaan->judul) }}" placeholder="Enter judul">
                    @error('judul')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi',$pertanyaan->isi) }}" placeholder="isi">
                    @error('isi')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tanggaldibuat">Tanggal Dibuat</label>
                    <input type="text" class="form-control" id="tanggaldibuat" name="tanggaldibuat" placeholder="tanggal dibuat">
                  </div>
                  <div class="form-group">
                    <label for="tanggaldiperbaharui">Tanggal Diperbaharui</label>
                    <input type="text" class="form-control" id="tanggaldiperbaharui" name="tanggaldiperbaharui" placeholder="tanggal diperbaharui">
                  </div>
                  <div class="form-group">
                    <label for="profilid">Profil Id</label>
                    <input type="text" class="form-control" id="profilid" name="profilid" placeholder="profil id">
                  </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">create</button>
                </div>
              </form>
            </div>
        
      </div>
           
@endsection